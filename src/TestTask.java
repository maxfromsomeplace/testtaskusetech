import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class TestTask {
    @Test
    public void firstTask() {
        String fileName = "./resources/NumbersForFirstTask";
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(fileName))) {
            String value = reader.readLine();
            System.out.println(value);
            int[] arrNumbers = Arrays.stream(value.split(", ")).mapToInt(Integer::parseInt).toArray();
            boolean needIteration = true;
            while (needIteration) {
                needIteration = false;
                for (int i = 1; i < arrNumbers.length; i++) {
                    if (arrNumbers[i] < arrNumbers[i - 1]) {
                        int tmp = arrNumbers[i];
                        arrNumbers[i] = arrNumbers[i-1];
                        arrNumbers[i-1] = tmp;
                        needIteration = true;
                    }
                }
            }
            System.out.println(Arrays.toString(arrNumbers));
            arrNumbers = Arrays.stream(value.split(", ")).mapToInt(Integer::parseInt).toArray();
            needIteration = true;
            while (needIteration) {
                needIteration = false;
                for (int i = 1; i < arrNumbers.length; i++) {
                    if (arrNumbers[i] > arrNumbers[i - 1]) {
                        int tmp = arrNumbers[i];
                        arrNumbers[i] = arrNumbers[i-1];
                        arrNumbers[i-1] = tmp;
                        needIteration = true;
                    }
                }
            }
            System.out.println(Arrays.toString(arrNumbers));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void secondTask() throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "./resources/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://yandex.ru/");
        driver.findElement(By.xpath("//a[contains(text(), 'Маркет')]")).click();
        driver.findElement(By.xpath("//span[contains(text(), 'Электроника')]")).click();
        driver.findElement(By.xpath("//a[contains(text(), 'Мобильные телефоны')]")).click();
        driver.findElement(By.xpath("//span[contains(text(), 'Samsung')]")).click();
        driver.findElement(By.name("Цена от")).sendKeys("40000");
        Thread.sleep(3000);
        String title= driver.findElements(By.cssSelector("div.n-snippet-cell2__title > a")).get(0).getAttribute("title");
        driver.findElements(By.cssSelector("div.n-snippet-cell2__title > a")).get(0).click();
        Assertions.assertEquals(title, driver.findElements(By.xpath("//h1")).get(0).getText());

        driver.findElement(By.xpath("//span[contains(text(), 'Электроника')]")).click();
        driver.findElement(By.xpath("//a[contains(text(), 'Наушники и Bluetooth-гарнитуры')]")).click();
        driver.findElement(By.xpath("//span[contains(text(), 'Beats')]")).click();
        driver.findElement(By.name("Цена от")).sendKeys("17000");
        driver.findElement(By.name("Цена до")).sendKeys("25000");
        Thread.sleep(3000);
        title= driver.findElements(By.cssSelector("div.n-snippet-cell2__title > a")).get(0).getAttribute("title");
        driver.findElements(By.cssSelector("div.n-snippet-cell2__title > a")).get(0).click();
        Assertions.assertEquals(title, driver.findElements(By.xpath("//h1")).get(0).getText());
        driver.close();
    }
    @Test
    public void thirdTask() throws InterruptedException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String time = sdf.format(new Date());
        System.setProperty("webdriver.gecko.driver", "./resources/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toUpperCase();
        driver.get("https://yandex.ru/");
        String actualTitle = driver.getTitle().toUpperCase();
        driver.findElement(By.id("text")).sendKeys("Альфа Банк");
        driver.findElement(By.cssSelector("div.search2__button")).click();
        driver.findElement(By.xpath("//b[contains(text(), 'AlfaBank.ru')]")).click();
        Thread.sleep(2000);
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        driver.findElement(By.xpath("//a[contains(text(), 'Вакансии')]")).click();
        try(FileWriter writer = new FileWriter("./resources/ThirdTask" + "_" + time + "_" + browserName.toString() + "_" + actualTitle.toString(), true))
        {
            ArrayList<WebElement> text = new ArrayList<WebElement>(driver.findElements(By.xpath("//p")));
            for (int i=0; i<=2;i++)
            {
                writer.write(text.get(i).getText() + "\n");
            }
            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
        driver.close();
    }
}
